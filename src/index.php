<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

define('ROOT', __DIR__);
require_once(ROOT.'/Components/Router.php');

$value = "Everyone!";
$prjdb = new PDO('mysql:host=172.18.0.3;dbname=projectdb;charset=utf8mb4', 'root', 'docker');

$router = new Router();
$router->run();

$databaseTest = ($prjdb->query('SELECT * FROM movies'))->fetchAll(PDO::FETCH_OBJ);

function GetMonthNameFromNumber(int $month){
    $result = "";
    switch($month){
        case 1:{$result = "January"; break;}
        case 2:{$result = "February"; break;}
        case 3:{$result = "March"; break;}
        case 4:{$result = "April"; break;}
        case 5:{$result = "May"; break;}
        case 6:{$result = "June"; break;}
        case 7:{$result = "July"; break;}
        case 8:{$result = "August"; break;}
        case 9:{$result = "September"; break;}
        case 10:{$result = "October"; break;}
        case 11:{$result = "November"; break;}
        case 12:{$result = "December"; break;}
    }
    return $result;
}

function ProcessDate (string $date){
    $splitteDate = explode('-', $date);
    $month = (int)$splitteDate[1];
    $textMonth = GetMonthNameFromNumber($month);
    $result = $textMonth.' '.$splitteDate[2].' '.$splitteDate[0];
    return $result;
}

?>
<html>
    <body>
        <h1>Hello, <?= $value ?>!</h1>
        <?php foreach($databaseTest as $row): 
        $date = $row->date_production;
        ?>
            <h2>Title: <?= $row->title ?></h2>
            <h4>Genre: <?= $row->genre ?></h4>
            <p>Date: <?= ProcessDate($date) ?></p>
            <p>Description: <?= $row->description ?></p>
        <?php endforeach; ?>
    </body>
</html>