<?php

Class Router {

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);
    }

    private function GetURI(){
        if(!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run (){
        $uri = $this->GetURI();
        $tempRoutes = $this->routes;
        $controller = "";
        foreach($tempRoutes as $key => $value){
            if($uri === $value){
                $controller = $key;
                break;
            }
        }
        $className = ucfirst($controller)."Controler";
        $path = ROOT.'/Controllers/' . $className . '.php';
        include($path);
        $object = new $className();
        $object->actionIndex();
    }

}