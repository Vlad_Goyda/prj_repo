<!DOCTYPE html>
<html lang="uk">

<head>
    <title>Crypthography - Lab2</title>
</head>

<body>
    <?php
    error_reporting(E_ERROR | E_PARSE);
    if (isset($_POST['encrypt']) and $_POST['opentext'] != null  and $_POST['key'] != null) {

        $key = str_split($_POST['key']);
        $keyLength = mb_strlen($_POST['key']);
        // $keyMass = str_split($key);
        $text = $_POST['opentext'];

        for ($i = 0; $i < (mb_strlen($text) % $keyLength); $i++)
            $text .= '*';
        $septext = mb_str_split($text);
        for ($i = 0; $i < mb_strlen($text); $i += $keyLength) {
            $trans = array();
            for ($j = 0; $j < $keyLength; $j++) {
                $poskey = $key[$j];
                $pos = $poskey - 1;
                $trans[$pos] = $septext[$i + $j];
            }
            for ($f = 0; $f < $keyLength; $f++) {
                $encrypted .= $trans[$f];
            }
        }
    }

    if (isset($_POST['decrypt']) and $_POST['crypto'] != null  and $_POST['key'] != null) {

        $key = str_split($_POST['key']);
        $keyLength = mb_strlen($_POST['key']);
        $text = $_POST['crypto'];
        $septext = mb_str_split($text);

        for ($i = 0; $i < mb_strlen($text); $i += $keyLength) {
            for ($j = 0; $j < $keyLength; $j++) {
                $decrypted .= $septext[$i + $key[$j] - 1];
            }
        }
    }
    ?>

    <p><strong>Перестановочний шифр</strong></p>

    <form action="Lab2.php" method="POST" autocomplete="off">
        <p><?php if (isset($_POST['encrypt']) and $_POST['opentext'] == null) echo "Введіть текст!"; ?></p>
        <p>Введiть текст для шифрування:<input type="textarea" name="opentext"></p>
        <p><?php if (isset($_POST['encrypt']) and $_POST['key'] == null) echo "Введіть ключ!"; ?></p>
        <p>Введiть ключ:<input type="text" name="key"></p>
        <input type="submit" name="encrypt" value="Пітвердити">
    </form>

    <?php if (isset($_POST['encrypt']) and $_POST['opentext'] != null and $_POST['key'] != null) { ?>
        <p>Зашифрований текст: <?php if (isset($_POST['opentext'])) echo $encrypted ?></p>
    <?php
    } ?>

    <form action="Lab2.php" method="POST" autocomplete="off">
        <p><?php if (isset($_POST['decrypt']) and $_POST['crypto'] == null) echo "Введіть текст!"; ?></p>
        <p>Введiть текст для дешифрування:<input type="textarea" name="crypto"></p>
        <p><?php if (isset($_POST['decrypt']) and $_POST['key'] == null) echo "Введіть ключ!"; ?></p>
        <p>Введiть ключ:<input type="text" name="key"></p>
        <input type="submit" name="decrypt" value="Пітвердити">
    </form>

    <?php if (isset($_POST['decrypt']) and $_POST['crypto'] != null) { ?>
        <p>Розшифрований текст: <?php echo $decrypted ?></p>
    <?php
    } ?>

</body>

</html>