<!DOCTYPE html>
<html lang="uk">

<head>
	<title>Crypthography - Lab1</title>
</head>

<body>
	<?php
	if (isset($_POST['opentext']) and isset($_POST['encrypt'])) {
		$text = mb_str_split($_POST['opentext']);
		$encrypted = "";
		$Lltr = array(0 => ' ', 1 => 'а', 2 => 'б', 3 => 'в', 4 => 'г', 5 => 'д', 6 => 'е', 7 => 'є', 8  => 'ж', 9 => 'з', 10 => 'и', 11 => 'і', 12 => 'ї', 13 => 'й', 14 => 'к', 15 => 'л', 16 => 'м', 17 => 'н', 18 => 'о', 19 => 'п', 20 => 'р', 21 => 'с', 22 => 'т', 23 => 'у', 24 => 'ф', 25 => 'х', 26 => 'ц', 27 => 'ч', 28 => 'ш', 29 => 'щ', 30 => 'ь', 31 => 'ю', 32 => 'я');
		$Hltr = array(0 => ' ', 1 => 'А', 2 => 'Б', 3 => 'В', 4 => 'Г', 5 => 'Д', 6 => 'Е', 7 => 'Є', 8  => 'Ж', 9 => 'З', 10 => 'И', 11 => 'І', 12 => 'Ї', 13 => 'Й', 14 => 'К', 15 => 'Л', 16 => 'М', 17 => 'Н', 18 => 'О', 19 => 'П', 20 => 'Р', 21 => 'С', 22 => 'Т', 23 => 'У', 24 => 'Ф', 25 => 'Х', 26 => 'Ц', 27 => 'Ч', 28 => 'Ш', 29 => 'Щ', 30 => 'Ь', 31 => 'Ю', 32 => 'Я');
		for ($i = 0; $i < count($text); $i++) {
			$index = array_search($text[$i], $Lltr);
			if ($index !== false) {
				$index = ($index + 5) % 32;
				$encrypted .= $Lltr[$index];
			} else {
				$index = array_search($text[$i], $Hltr);
				if ($index !== false) {
					$index = ($index + 5) % 32;
					$encrypted .= $Hltr[$index];
				}
			}
		}
	}
	?>
	<?php
	if (isset($_POST['crypt']) and isset($_POST['decrypt'])) {
		$text = mb_str_split($_POST['crypt']);
		$decrypted = "";
		$Lltr = array(0 => ' ', 1 => 'а', 2 => 'б', 3 => 'в', 4 => 'г', 5 => 'д', 6 => 'е', 7 => 'є', 8  => 'ж', 9 => 'з', 10 => 'и', 11 => 'і', 12 => 'ї', 13 => 'й', 14 => 'к', 15 => 'л', 16 => 'м', 17 => 'н', 18 => 'о', 19 => 'п', 20 => 'р', 21 => 'с', 22 => 'т', 23 => 'у', 24 => 'ф', 25 => 'х', 26 => 'ц', 27 => 'ч', 28 => 'ш', 29 => 'щ', 30 => 'ь', 31 => 'ю', 32 => 'я');
		$Hltr = array(0 => ' ', 1 => 'А', 2 => 'Б', 3 => 'В', 4 => 'Г', 5 => 'Д', 6 => 'Е', 7 => 'Є', 8  => 'Ж', 9 => 'З', 10 => 'И', 11 => 'І', 12 => 'Ї', 13 => 'Й', 14 => 'К', 15 => 'Л', 16 => 'М', 17 => 'Н', 18 => 'О', 19 => 'П', 20 => 'Р', 21 => 'С', 22 => 'Т', 23 => 'У', 24 => 'Ф', 25 => 'Х', 26 => 'Ц', 27 => 'Ч', 28 => 'Ш', 29 => 'Щ', 30 => 'Ь', 31 => 'Ю', 32 => 'Я');
		for ($i = 0; $i < count($text); $i++) {
			$index = array_search($text[$i], $Lltr);
			if ($index !== false) {
				$index -= 5;
				if ($index < 0) {
					$index += 32;
				}
				$index %= 32;
				$decrypted .= $Lltr[$index];
			} else {
				$index = array_search($text[$i], $Hltr);
				if ($index !== false) {
					$index -= 5;
					if ($index < 0) {
						$index += 32;
					}
					$index %= 32;
					$decrypted .= $Hltr[$index];
				}
			}
		}
	}
	?>
	<p><strong>Шифр Цезаря</strong></p>
	<form action="Lab1.php" method="POST" autocomplete="off">
		<p><?php if (isset($_POST['encrypt']) and $_POST['opentext'] == null) echo "Введіть текст!"; ?></p>
		<p>Введiть текст для шифрування:<input type="textarea" name="opentext"></p>
		<input type="submit" name="encrypt" value="Пітвердити">
	</form>
	<?php if (isset($_POST['encrypt']) and $_POST['opentext'] != null) { ?>
		<p>Зашифрований текст: <?php if (isset($_POST['opentext'])) echo $encrypted ?></p>
	<?php
	} ?>
	<form action="Lab1.php" method="POST" autocomplete="off">
		<p><?php if (isset($_POST['decrypt']) and $_POST['crypt'] == null) echo "Введіть текст!"; ?></p>
		<p>Введiть текст для дешифрування:<input type="textarea" name="crypt"></p>
		<input type="submit" name="decrypt" value="Пітвердити">
	</form>
	<?php if (isset($_POST['decrypt']) and $_POST['crypt'] != null) { ?>
		<p>Розшифрований текст: <?php if (isset($_POST['crypt'])) echo $decrypted ?></p>
	<?php
	} ?>
</body>

</html>