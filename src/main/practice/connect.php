<?php
$host = 'localhost'; //host name, default - localhost
$user = 'root'; //MySQL username
$password = 'toor'; //MySQL password, may be null
$db_name = 'phpdb'; //DB Name
$link = mysqli_connect($host, $user, $password, $db_name) or die(mysqli_error($link));

//Устанавливаем кодировку (не обязательно, но поможет избежать проблем):
mysqli_query($link, "SET NAMES 'utf8'");