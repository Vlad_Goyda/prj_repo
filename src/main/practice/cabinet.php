<?php session_start();?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="vviewport" content="width=device-width, 
        user-scalable=no, 
        initial-scale=1.0, 
        maximum-scale=1.0, 
        minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=7">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">

    <title>Cabinet</title>
</head>

<body>
    <div class="container">       
        <h1 class="text">Greetings, <?php $username = $_SESSION['username']; echo $username ?>!</h1>
        <p class="text">Sorry, but there isn`t any content on this webpage. Please, log out.</p>
        <form action="index.php">
            <input type="submit" name="logout">
            <a class="btn btn-lg btn-outline-primary btn-block" href="index.php" role="button">Log out</a>
        </form>
    </div>
</body>

</html>