<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="vviewport" content="width=device-width, 
        user-scalable=no, 
        initial-scale=1.0, 
        maximum-scale=1.0, 
        minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=7">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">

    <title>Practice</title>
</head>
<?php
//require('connect.php');
$host = 'localhost'; //host name, default - localhost
$user = 'root'; //MySQL username
$password = 'toor'; //MySQL password, may be null
$db_name = 'phpdb'; //DB Name
$link = mysqli_connect($host, $user, $password, $db_name) or die(mysqli_error($link));

if (isset($_POST['username']) and isset($_POST['email']) and isset($_POST['password']) and isset($_POST['name'])) {
    $username = $_POST['username'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];

    $query = "INSERT INTO users SET UserLogin='$username', UserName='$name', Email='$email', UserPassword='$password', IsActive=0";
    $result = mysqli_query($link, $query);

    if ($result) {
        $_SESSION['username'] = $username;
        $smsg = "Successful registation. Greetings, " . $name . " !";
        header("Location: http://localhost/practice/cabinet.php");
        die();
    } else {
        $fmsg = "Error! Account wasn`t registered";
    }
}
?>

<body>
    <div class="container">
        <form class="form-signin" method="post" autocomplete="off">
            <h2>Registration</h2>
            <?php
            if (isset($smsg)) { ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $smsg ?>
                </div>
            <?php
            } else if (isset($fmsg)) { ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $fmsg ?>
                </div>
            <?php
            }
            ?>
            <input type="text" name="username" placeholder="Username" required>
            <input type="text" name="name" placeholder="Name" required>
            <input type="email" name="email" placeholder="Email" required>
            <input type="password" name="password" placeholder="Password" required>
            <button class="btn btn-lg btn-outline-primary btn-block" type="submit">Registration</button>
        </form>
    </div>
</body>

</html>