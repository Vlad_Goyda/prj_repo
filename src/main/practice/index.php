<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="vviewport" content="width=device-width, 
        user-scalable=no, 
        initial-scale=1.0, 
        maximum-scale=1.0, 
        minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=7">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">

    <title>Practice</title>
</head>
<?php

require('connect.php');
/*$host = 'localhost'; //host name, default - localhost
$user = 'root'; //MySQL username
$password = 'toor'; //MySQL password, may be null
$db_name = 'phpdb'; //DB Name
$link = mysqli_connect($host, $user, $password, $db_name) or die(mysqli_error($link));*/

if (isset($_POST['login']) and isset($_POST['password'])) {
    $username = $_POST['login'];
    $password = $_POST['password'];

    $query = "SELECT * FROM users WHERE UserLogin='$username' AND UserPassword='$password'";
    $result = mysqli_query($link, $query);

    if ($result) {
        $smsg = "Greetings, " . $username . " !";
        $query = "UPDATE SET IsActive=1 WHERE UserLogin='$username'";
        $result = mysqli_query($link, $query);
        $_SESSION['username'] = $username;
        header("Location: http://localhost/practice/cabinet.php");
        die();
    } else {
        $fmsg = "Error! Invalid username or password";
    }
}
?>

<body>
    <div class="container">
        <form class="form-signin" method="post" autocomplete="off">
            <h2>Login</h2>
            <?php
            if (isset($smsg)) { ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $smsg ?>
                </div>
            <?php
            } else if (isset($fmsg)) { ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $fmsg ?>
                </div>
            <?php
            }
            ?>
            <input type="text" name="login" placeholder="Login" required>
            <input type="password" name="password" placeholder="Password" required>
            <div class="d-grid gap-2 col-6 mx-auto">
                <button class="btn btn-lg btn-outline-secondary btn-block" type="submit">Login</button>
                <a class="btn btn-lg btn-outline-primary btn-block" href="/practice/registrat.php" role="button">Registration</a>
            </div>
        </form>
    </div>
</body>

</html>