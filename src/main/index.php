<!DOCTYPE html>
<html lang="uk">

<head>
	<meta charset="UTF-8">
	<meta name="vviewport" content="width=device-width, 
        user-scalable=no, 
        initial-scale=1.0, 
        maximum-scale=1.0, 
        minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=7">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<title>Main page</title>
</head>

<body>
	<div class="container">
		<a href="Lab1.php"><button class="btn btn-lg btn-default btn-block">Lab1</button></a>
		<a href="Lab2.php"><button class="btn btn-lg btn-default btn-block">Lab2</button></a>
		<a href="/practice/index.php"><button class="btn btn-lg btn-primary btn-block">Practice</button></a>
	</div>
</body>

</html>